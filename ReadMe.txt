##Hitori Game C++

L'Hitori è costituito da una griglia di caselle quadrate,
inizialmente riempite da numeri.
Lo scopo del gioco è quello di annerirne alcune, in modo tale che non compaiano
numeri ripetuti in ciascuna riga o colonna.

Il gioco carica da file matrici quadrate di dimensione prefissata 5x5.
Nella cartella principale è già presente una matrice 5x5 di prova.

Il progetto è stato scritto in C++, facendo l'utilizzo della libreria Qtcreator
per la realizzazione della GUI.

Il file della matrice deve essere cosi strutturato per funzionare:
*Primo numero = numero righe
*Secondo numero = numero colonne
*Di seguito gli altri 25 numeri che compongono la matrice.