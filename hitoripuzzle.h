#ifndef HITORIPUZZLE_H
#define HITORIPUZZLE_H

#include <iostream>
#include <vector>

using namespace std;

class HitoriPuzzle
{
public:

    HitoriPuzzle();

    void mark_black(int x, int y);
    void mark_white(int x, int y);
    void mark_circled(int x, int y);
    void clean_vector();
    void clean_ghost_and_annotation();
    void change_vector_numbers (int number);
    void change_cols_number (int new_cols);
    void change_rows_number (int new_rows);
    void change_dimension_vector(int dimension);
    void automatic_circle();
    void ghost_number();
    void automatic_black();

    bool winner ();
    bool wrong(int x, int y);
    bool is_white(int x,int y);
    bool can_walk(int x, int y, vector<int> *control);
    bool is_black(int x, int y);
    bool is_circled(int x, int y);
    bool is_white_position(int pos);

    int number_cols();
    int number_rows();  
    int get_number(int x, int y);
    int get_number_ghost(int x, int y);
    int number_of_not_white();
    int number_of_white();
    int number_of_black();
    int tunnel (int x, int y,vector<int>*control);  
    int help_cols();
    int help_rows();

    string str();

private:

    int cols = 5;
    int rows = 5;

    vector<int> numbers_ = {1, 5, 3, 1, 2,
                            5, 4, 1, 3, 4,
                            3, 4, 3, 1, 5,
                            4, 4, 2, 3, 3,
                            2, 1, 5, 4, 4};

    vector<int> annotations_ = { 0, 0, 0, 0, 0,
                                 0, 0, 0, 0, 0,
                                 0, 0, 0, 0, 0,
                                 0, 0, 0, 0, 0,
                                 0, 0, 0, 0, 0 };

    vector<int> ghost_ = { 0, 0, 0, 0, 0,
                           0, 0, 0, 0, 0,
                           0, 0, 0, 0, 0,
                           0, 0, 0, 0, 0,
                           0, 0, 0, 0, 0 };

                };



#endif // HITORIPUZZLE_H

