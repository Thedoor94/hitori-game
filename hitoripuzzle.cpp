#include "hitoripuzzle.h"
#define white 0
#define black 1
#define step_on 6
#define circled 9
#define ghost 8

HitoriPuzzle::HitoriPuzzle()
{
}

void HitoriPuzzle::mark_white(int x, int y)
{
    annotations_[x + y * cols ] = white;
}

void HitoriPuzzle::mark_black(int x, int y)
{
    annotations_[x + y * cols ] = black;

}

void HitoriPuzzle::mark_circled(int x, int y)
{
    annotations_[x + y * cols ] = circled;
}

int HitoriPuzzle::number_cols()
{
    return cols;
}

int HitoriPuzzle::number_rows()
{
    return rows;
}

bool HitoriPuzzle::winner()
{
    //per vincere l'utente deve annerire i numeri doppi
    //nelle colonne e nelle righe e cerchiare tutti i numeri rimanenti

    //controllo doppi colonne
    for (int x=0; x<rows; ++x) {
        for (int y=0; y<cols; ++y) {
            for (int i=y; i<cols-1; i++) {
                if (is_white_position(x+(y*cols)) && is_white_position(x + (i+1) * cols)) {
                    if (numbers_[x+(y*cols)] == numbers_[x + (i+1) * cols]) {
                        return false;
                    }
                }
            }
        }
    }

    //controllo doppi righe
    for (int y=0; y<rows; ++y) {
        for (int x=0; x<cols; ++x) {
            for (int i=x; i<cols-1; i++) {
                if (is_white_position(x+(y*cols)) && is_white_position((i+1) + (y * cols))) {
                    if (numbers_[x+(y*cols)] == numbers_[(i+1) + (y * cols)]) {
                        return false;
                    }
                }
            }
        }
    }
    if (number_of_white() != 0) {
        return false;
    }
    cout<< "Hai vinto!!"<<endl;
    return true;

}

void HitoriPuzzle::automatic_circle()
{
    for (int x=0; x<rows; ++x) {
        for (int y=0; y<cols; ++y) {
            if (x>0 and x<(rows-1)) {
                if (annotations_ [x + (y * cols) ]==black ){
                    annotations_ [(x+1) + (y * cols) ]=circled;
                    annotations_ [(x-1) + (y * cols) ]=circled;
                    annotations_ [x + ((y-1) * cols) ]=circled;
                    annotations_ [x + ((y+1) * cols) ]=circled;

                }
            }
            if (x==0) {
                if (annotations_ [x + (y * cols) ]==black ){
                    annotations_ [(x+1) + (y * cols) ]=circled;
                    annotations_ [x + ((y-1) * cols) ]=circled;
                    annotations_ [x + ((y+1) * cols) ]=circled;
                }
            }
            if (x==(rows-1)) {
                if (annotations_ [x + (y * cols) ]==black ){
                    annotations_ [(x-1) + (y * cols) ]=circled;
                    annotations_ [x + ((y-1) * cols) ]=circled;
                    annotations_ [x + ((y+1) * cols) ]=circled;
                }
           }
        }
    }
}

void HitoriPuzzle::ghost_number()
{
    // ghost number li uso per far sparire i numeri "inutili" sulla matrice

    //colonne
    for (int x=0; x<rows; ++x) {
        for (int y=0; y<cols; ++y) {
            for (int i=y; i<cols-1; i++) {
                if (is_white_position(x+(y*cols)) && is_white_position(x + (i+1) * cols)) {
                    if (numbers_[x+(y*cols)] == numbers_[x + (i+1) * cols]) {
                        ghost_[x+(y*cols)] = ghost;
                        ghost_[x + (i+1) * cols] = ghost;
                    }
                }
            }
        }
    }

    //righe
    for (int y=0; y<rows; ++y) {
        for (int x=0; x<cols; ++x) {
            for (int i=x; i<cols-1; i++) {
                if (is_white_position(x+(y*cols)) && is_white_position((i+1) + (y * cols))) {
                    if (numbers_[x+(y*cols)] == numbers_[(i+1) + (y * cols)]) {
                        ghost_[x+(y*cols)] = ghost;
                        ghost_[(i+1) + (y * cols)] = ghost;
                    }
                }
            }
        }
    }
}

void HitoriPuzzle::automatic_black()
{
    for (int x=0; x<rows; ++x) {
        for (int y=0; y<cols; ++y) {
            if (annotations_ [x + (y * cols) ]==circled && annotations_ [(x+2) + (y * cols) ]==circled ){
                    annotations_ [(x+1) + (y * cols) ]=black;
            }
            if (annotations_ [x + (y * cols) ]==circled && annotations_ [x + ((y+2) * cols) ]==circled ){
                    annotations_ [x+ + ((y+1) * cols) ]=black;
            }
        }
    }
}

string HitoriPuzzle::str()
{
    // stampa della matrice (solo per riga di comando)
    string matrix;
    for (int x=0; x<rows; ++x) {
        for (int y =0; y < cols ; ++y) {
            matrix.append(to_string(numbers_[y + (x*cols)]))    ;
            if (annotations_[y + (x*cols)] == black) {
                matrix.append("#");
            }

            else if (annotations_[y + (x*cols)] == circled) {
                matrix.append("!");
            }
            else {
                matrix.append(" ");
            }
            matrix.append(" ");
        }
        matrix.append("\n");

    }
    return matrix;
}

bool HitoriPuzzle::is_black(int x, int y)
{
    if (annotations_ [x + (y * cols) ]==black ){
        return true;
    }
    return false;
}

bool HitoriPuzzle::is_white_position(int pos)
{
    if (annotations_ [pos]==white ){
        return true;
    }
    return false;
}

int HitoriPuzzle::get_number(int x, int y)
{
    return numbers_[x + (y * cols)];
}

int HitoriPuzzle::get_number_ghost(int x, int y)
{
     return ghost_[x + (y * cols)];
}

int HitoriPuzzle::number_of_not_white()
{
    int cont=0;
    for (int i=0; i<annotations_.size(); ++i) {
            if (annotations_[i]!= black) {
                cont++;
            }
        }
    return cont;
}

int HitoriPuzzle::number_of_white()
{
    int cont=0;
    for (int i=0; i<annotations_.size(); ++i) {
            if (annotations_[i]== white) {
                cont++;
            }
        }
    return cont;
}

int HitoriPuzzle::number_of_black()
{
    int cont=0;
    for (int i=0; i<annotations_.size(); ++i) {
            if (annotations_[i]== black) {
                cont++;
            }
        }
    return cont;
}

bool HitoriPuzzle::is_circled(int x, int y)
{   bool circle=false;
        if( annotations_[x + y * cols ] == circled ){
            circle=true;
    }
        return circle;
}

bool HitoriPuzzle::is_white(int x, int y)
{
    if (annotations_ [x + y * cols ]==white ){
        return true;
    }
    return false;
}

bool HitoriPuzzle::can_walk(int x, int y, vector<int> *control)
{
    if ((*control) [x + y * cols ] == white ){
        return true;
    }
    return false;
}

int HitoriPuzzle::tunnel(int x, int y,vector<int> *control)
{
    int cont = 1;

    (*control)[x + y * cols ] = step_on;

    if (can_walk(x+1,y, control)){
        if (x<(rows-1) and x>=0) {
            cont+=tunnel(x+1,y, control);
        }
    }
    if (can_walk(x-1,y, control)){
        if (x<(rows-1) and x>=1) {
            cont+= tunnel(x-1,y, control);
        }
    }
    if (can_walk(x,y+1, control)){
        if (y<(cols-1) and y>=0) {
            cont+= tunnel(x,y+1, control);
        }
    }
    if (can_walk(x,y-1, control)){
        if (y<cols and y>=1) {
            cont+= tunnel(x,y-1, control);
        }
    }

    return cont;
}

bool HitoriPuzzle::wrong(int x, int y)
{
    vector<int> control;

    for (int i = 0;i<annotations_.size();++i) {
        if (annotations_[i]==black) {
            control.push_back(1);
        }
        else {
            control.push_back(0);
        }
    }

    if (is_black(x+1,y)|| is_black(x-1,y)|| is_black(x,y+1) || is_black(x,y-1) ) {
        return true;
    }

    if (x==0 && y==0) {
        if (tunnel(0,1,&control)!= number_of_not_white()) {
            return true;
        }
    }
    else {
        if (is_circled(x,y)) {
            if (tunnel(0,1,&control)!= number_of_not_white()) {
                return true;
            }
        }
        else {
            if (tunnel(0,0,&control)!= number_of_not_white()) {
                return true;
            }
        }
    }
    return false;
}

void HitoriPuzzle::clean_vector()
{
    numbers_.clear();
}

void HitoriPuzzle::clean_ghost_and_annotation()
{
    ghost_.clear();
    annotations_.clear();
    for (int i = 0; i < (cols*rows); ++i) {
       ghost_.push_back(0);
       annotations_.push_back(0);
    }
}

void HitoriPuzzle::change_vector_numbers(int number)
{
    numbers_.push_back(number);
}

void HitoriPuzzle::change_rows_number(int new_rows)
{
    rows= new_rows;
}

void HitoriPuzzle::change_dimension_vector(int dimension)
{
    ghost_.clear();
    annotations_.clear();
    for (int i = 0; i < dimension; ++i) {
       ghost_.push_back(0);
       annotations_.push_back(0);
    }
}

void HitoriPuzzle::change_cols_number(int new_cols)
{
    cols= new_cols;
}

int HitoriPuzzle::help_cols()
{
    int wrong_number=100;
    //colonne
    for (int x=0; x<rows; ++x) {
        for (int y=0; y<cols; ++y) {
            for (int i=y; i<cols-1; i++) {
                if (is_white_position(x+(y*cols)) && is_white_position(x + (i+1) * cols)) {
                    if (numbers_[x+(y*cols)] == numbers_[x + (i+1) * cols]) {
                        return x; // ritorna l'indice della colonna per l'indizio
                    }
                }
            }
        }
    }
    return wrong_number;
}

int HitoriPuzzle::help_rows()
{
    int wrong_number=100;
    //righe
    for (int y=0; y<rows; ++y) {
        for (int x=0; x<cols; ++x) {
            for (int i=x; i<cols-1; i++) {
                if (is_white_position(x+(y*cols)) && is_white_position((i+1) + (y * cols))) {
                    if (numbers_[x+(y*cols)] == numbers_[(i+1) + (y * cols)]) {
                        return y; //ritorna l'indice della riga per l'indizio
                    }
                }
            }
        }
    }
    return wrong_number;
}
