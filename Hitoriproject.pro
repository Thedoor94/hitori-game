QT+= core gui widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Hitoriproject

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG+=C++11

SOURCES += main.cpp \
    hitoripuzzle.cpp \
    gamegui.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    hitoripuzzle.h \
    gamegui.h

