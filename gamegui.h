#ifndef GAMEGUI_H
#define GAMEGUI_H

#include "hitoripuzzle.h"
#include <QPushButton>
#include <QWidget>

class gamegui : public QWidget
{
    Q_OBJECT

public:
    gamegui(HitoriPuzzle* hitori);
    bool show_ghost=true;

private:

    void model_graphics();
    void click_button(int x, int y);
    void update_button(int x, int y);
    void update_button_ghost(int x, int y);
    void update_all_buttons();
    void update_all_buttons_ghost();
    void finish();
    void help();
    void open_new_matrix();

    //metodi dichiarati nel header perchè molto corti
    int number_rows() { return hitori_->number_rows(); }
    int number_cols() { return hitori_->number_cols(); }

    vector<QPushButton*> buttons_;

    QPushButton* ghost_button;
    QPushButton* finish_button;
    QPushButton* auto_black_button;
    QPushButton* auto_circled_button;
    QPushButton* help_button;
    QPushButton* open_button;
    QPushButton* restart_button;

    HitoriPuzzle* hitori_;
};


#endif // GAMEGUI_H
