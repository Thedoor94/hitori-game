#include <iostream>
#include <vector>
#include "hitoripuzzle.h"
#include "gamegui.h"
#include <QApplication>
using namespace std;

int main(int argc, char * argv [])
{
    QApplication a{argc, argv};
    HitoriPuzzle * mypuzzle = new HitoriPuzzle;
    gamegui gui(mypuzzle);

    return a.exec();
}

// main per far partire hitori a riga di comando senza grafica
int main_cmd()
{
    int x,y;
    bool c = false;
    char scelta;

    HitoriPuzzle hitoripuzzle;

    while (c == false) {
        cout << " "<<endl;
        cout << hitoripuzzle.str() << endl;
        cout<< "Cosa vuoi fare? \n (a = annerisci cella, c = cerchia cella, \n d= disannerisci cella, t= cerchia automaticamente le celle \n z= annerisci automaticamente le celle)"<< endl;
        cin>> scelta;

        if (scelta == 'a') {
            cout<<"inserisci una coordinata x "<<endl;
            cin>>x;
            cout<<"inserisci una coordinata y"<<endl;
            cin >>y;
            if (hitoripuzzle.wrong(x,y)) {
                cout << "Non puoi annerire questa cella" << endl;
            }
            else {
                hitoripuzzle.mark_black(x,y);
            }

        }
        else if (scelta =='c') {
            cout<<"inserisci una coordinata x "<<endl;
            cin>>x;
            cout<<"inserisci una coordinata y"<<endl;
            cin >>y;
            if (hitoripuzzle.wrong(x,y) == false) {
                hitoripuzzle.mark_circled(x,y);
            }
        }
        else if (scelta == 'd'){
            cout<<"inserisci una coordinata x "<<endl;
            cin>>x;
            cout<<"inserisci una coordinata y"<<endl;
            cin >>y;
            if (hitoripuzzle.is_black(x,y)) {
                hitoripuzzle.mark_white(x,y);
            }
            else {
                cout<<"Questa cella non è nera"<<endl;
            }

        }
        else if (scelta == 't') {
            hitoripuzzle.automatic_circle();
        }
        else if (scelta == 'z') {
            hitoripuzzle.automatic_black();
        }
        else {
            cout << "Scelta errata" << endl;
        }
        c =hitoripuzzle.winner();
    }

    return 0;

}



