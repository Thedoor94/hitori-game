#include "gamegui.h"
#include "hitoripuzzle.h"
#include <QGridLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>
#include <fstream>
#include <sstream>

#define ghost 8
#define wrong_number 100

gamegui::gamegui(HitoriPuzzle* hitori)
{
    this->hitori_ = hitori;
    auto button_layout = new QVBoxLayout;

    ghost_button = new QPushButton{tr("Ghost")};
    finish_button = new QPushButton{tr("Finish")};
    auto_black_button = new QPushButton{tr("Auto black")};
    auto_circled_button = new QPushButton{tr("Auto circled")};
    help_button = new QPushButton{tr("Help")};
    open_button = new QPushButton {tr ("Open")};
    restart_button = new QPushButton {tr ("Restart")};

    auto grid = new QGridLayout;
    auto main_layout = new QHBoxLayout;

    button_layout->addWidget(ghost_button);
    button_layout->addWidget(finish_button);
    button_layout->addWidget(auto_black_button);
    button_layout->addWidget(auto_circled_button);
    button_layout->addWidget(help_button);
    button_layout->addWidget(open_button);
    button_layout->addWidget(restart_button);

    connect(auto_circled_button, &QPushButton::clicked,[=]{hitori_->automatic_circle();update_all_buttons();});
    connect(restart_button, &QPushButton::clicked,[=]{hitori_->clean_ghost_and_annotation();update_all_buttons();});
    connect(auto_black_button, &QPushButton::clicked,[=]{hitori_->automatic_black();update_all_buttons();});
    connect(finish_button, &QPushButton::clicked,[=]{finish();});
    connect(help_button, &QPushButton::clicked,[=]{help();});
    connect(open_button, &QPushButton::clicked,[=]{open_new_matrix();update_all_buttons();});
    connect(ghost_button, &QPushButton::clicked,[=]{hitori_->ghost_number();
        if (show_ghost) {update_all_buttons_ghost();show_ghost=false;}
        else {update_all_buttons(); show_ghost=true;}});

    button_layout->addStretch();  // per mettere bottoni ordinati

    main_layout->addLayout(grid);
    main_layout->addLayout(button_layout);

    setLayout(main_layout);

    // creazione bottoni dinamicamente
    for (int x = 0; x < number_rows(); ++x) {
        for (int y = 0; y < number_cols(); ++y) {
            auto button = new QPushButton;
            button->setMinimumWidth(50);
            button->setMinimumHeight(50);
            buttons_.push_back(button);
            grid->addWidget(button, y, x);
            connect(button, &QPushButton::clicked,[=]{click_button(x, y);});
        }
    }
    model_graphics();
    update_all_buttons();
}

void gamegui::update_button(int x, int y)
{
    auto button = buttons_[y + (x * number_cols())];
    button->setText(QString::number(hitori_->get_number(x, y)));

    if (hitori_-> is_black(x,y)) {
       button->setStyleSheet("background: black; color: white");
    }

    else if (hitori_->is_circled(x,y)) {
       button->setStyleSheet("background: green; color: white");
    }

    else {
       button->setStyleSheet("background: white; color: black");
    }
}

void gamegui::update_all_buttons() {
    for (int x = 0; x < number_rows(); ++x) {
        for (int y = 0; y < number_cols(); ++y) {
            update_button(x, y);
        }
    }
}

void gamegui::update_button_ghost(int x, int y)
{
    auto button = buttons_[y + (x * number_cols())];
    if (hitori_->get_number_ghost(x,y) != ghost) {
        hitori_->mark_white(x,y);
        button->setStyleSheet("background: white; color: black");
        button->setText(" ");

    }
    else {
        button->setText(QString::number(hitori_->get_number(x, y)));
    }
}

void gamegui::update_all_buttons_ghost() {
    for (int x = 0; x < number_rows(); ++x) {
        for (int y = 0; y < number_cols(); ++y) {
            update_button_ghost(x, y);
        }
    }
}

void gamegui::model_graphics() {

    setWindowTitle("Hitori c++");
    layout()->setMargin(0);
    layout()->setSpacing(0);
    layout()->setSizeConstraint(QLayout::SetFixedSize);
    adjustSize();
    show();

}

void gamegui::finish()
{
    if (hitori_->winner()) {

        auto button = QMessageBox::question(
                    this,
                    tr("Hai vinto!"),
                    tr("Complimenti hai completato il puzzle"),
                    QMessageBox::Ok);

        if (button == QMessageBox::Ok) {
            window()->close();
        }
    }
    else {
        QMessageBox::question(
                    this,
                    tr("Warning"),
                    tr("Non hai finito la partita,esegui altre mosse"),
                    QMessageBox::Ok);
    }
}


void gamegui::help()
{
    if (hitori_->help_cols() != wrong_number) {
        QMessageBox::information(
            this,
            tr("Help"),
            QString("Puoi annerire una cella nella colonna: %1").arg(hitori_->help_cols()));
    }
    else if(hitori_->help_rows() != wrong_number) {
        QMessageBox::information(
            this,
            tr("Help"),
            QString("Puoi annerire una cella nella riga: %1").arg(hitori_->help_rows()));
    }

    else {
        QMessageBox::information(
                    this,
                    tr("Help"),
                    tr("Non posso darti ulteriori aiuti"));
    }
}

void gamegui::click_button(int x, int y){

    if (hitori_->is_white(x,y)) {
        hitori_->mark_circled(x,y);
    }
    else if (hitori_->is_circled(x,y)) {
        hitori_->mark_black(x,y);
        bool c= hitori_->wrong(x,y);
        if (c) {
            hitori_->mark_white(x,y);
            QMessageBox::information(
                this,
                tr("Warning"),
                tr("Non puoi annerire questa cella!"));
        }
    }
    else if (hitori_->is_black(x,y)) {
        hitori_->mark_white(x,y);
    }

    update_button(x,y);
}

void gamegui::open_new_matrix() {

    auto filename = QFileDialog::getOpenFileName(this);
    if (filename != "") {
        ifstream file{filename.toStdString()};
        if (file.good()) {
            string content;
            getline(file, content, '\0');
            hitori_->clean_vector();
            for (int n = 0; n < content.size(); n++)
            {
                if (n==0) {
                    char c= content[n];
                    int t= c - '0';   //convert the character '0' -> 0 int
                    hitori_->change_cols_number(t);
                }
                else if (n==1) {
                    char c= content[n];
                    int t= c - '0';   //convert the character '0' -> 0 int
                    hitori_->change_rows_number(t);
                }
                else {
                    char c= content[n];
                    int t= c - '0';   //convert the character '0' -> 0 int
                    hitori_->change_vector_numbers(t);
                }
            }
            hitori_-> change_dimension_vector((content.size()-2));
        } else {
            QMessageBox::critical(this, tr("Error"),
                                  tr("Could not open file"));
        }
    }
}

